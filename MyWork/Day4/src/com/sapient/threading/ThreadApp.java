package com.sapient.threading;


class CA{
	
	
}


// MyTask IS-a thread
class MyTask extends CA implements Runnable{
	
	public void run() {
		for(int doc=1;doc<=10;doc++)
			System.out.println("Prinitng doc #"+doc+ " Printer-2");
		
		
	}
}

class YourTask extends CA implements Runnable{
	
	public void run() {
		for(int doc=1;doc<=10;doc++)
			System.out.println("Prinitng doc #"+doc+ " Printer-3");
		
		
	}
}

public class ThreadApp {

	
	// main method represents main thread
	public static void main(String[] args) {
		// whatever we write in here will be executed by main thread
		// threads always execute the jobs in a seq
		// Execution Context
		
		//Job1
		System.out.println("App started");
		
		//Job2
		//MyTask myTask = new MyTask();  // child thread / worker thread
		//myTask.start();  // start shall internally execute the run method 
		
		Runnable r  = new MyTask();
		Thread task = new Thread(r);
		task.start();
		
		
		Runnable r1  = new YourTask();
		Thread ytask = new Thread(r1);
		ytask.start();
		
		
		// till job2 is not finished , below jobs will wait  and not executed
		
		//in case job2 is long running operation
		// in such a use case  OS/JVM shall give a message that app  is not responding
		// some sluggish behaviour in the app can be observed->app seems to hang bcz
		// main thread is blocked 
		
		
		// now main and MyTask are executing both parallely  or concurrently
		
		//Job3
		for(int doc=1;doc<=10;doc++)
			System.out.println("Prinitng doc #"+doc+" Printer-1");
		
		
		//Job4
		System.out.println("App end");
		
		

	}

}
