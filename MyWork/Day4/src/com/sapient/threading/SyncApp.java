package com.sapient.threading;

class Printer{
	
	
	synchronized void printDocuments(int numOfCopies, String docName) {
		for(int i=1;i<=numOfCopies;i++) {
			
			
			
			try {
				Thread.sleep(500);
				System.out.println("Printing " + docName+" "+i);
				notify();
				
				wait();
			}
			
			catch(InterruptedException e) {
				e.printStackTrace();
			}
            
			
			
			
		}
	}
}

class MyThread extends Thread{
	
	Printer pRef;
	MyThread(Printer p){
		
		pRef = p;
	}
	@Override
	public void run() {
		synchronized (pRef) {
			pRef.printDocuments(10,"John's Profile");
		}
		
	}
	
}

class YourThread extends Thread{
	
	Printer pRef;
	YourThread(Printer p){
		
		pRef = p;
	}
	@Override
	public void run() {
		synchronized (pRef) {
			pRef.printDocuments(10,"Kaira's Profile");
		}
		
	}
	
}

public class SyncApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("App started");
		
		// we have single object of printer
		Printer printer = new  Printer();
		//printer.printDocuments(10,"Ishant's profile pdf");
		
		
		// Scenario is that we have multiple thread working on the same Printer object
		// If multiple threads are going to work on the same single object we must synchronize them
		
		MyThread  mRef= new MyThread(printer);   // Mythread is having ref to printer object
		
		
		YourThread  yRef= new YourThread(printer);   // Yourthread is having ref to printer object
		mRef.start();
		/*try {
			mRef.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		yRef.start();
		System.out.println("App finished");
		
	}

}
