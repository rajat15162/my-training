package com.sapient.Day4Q1;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Employee2 {
	
	public static void main(String[] args) {
		
		Map<Integer, Set<String>> ob = new LinkedHashMap <Integer, Set<String>>();
		
		ob.put(101,new LinkedHashSet<String>());
		ob.get(101).add("B.E.");
		ob.get(101).add("M.E.");
		
		
		ob.put(102,new LinkedHashSet<String>());
		ob.get(102).add("B.E.");
	
		
		System.out.println(ob);
		System.out.println(ob.keySet());
		System.out.println(ob.values());
		
		
		
	}

}
