package com.sapient.student.others;

import java.util.ResourceBundle;

public class RBundle {

	public static String getValues(String key) {
		
		ResourceBundle rs = ResourceBundle.getBundle("student"); // student is the property file name
		return rs.getString(key);
	}
}
