package com.sapient.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InsertNewStudent
 */
public class InsertNewStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertNewStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		

		PrintWriter out = response.getWriter();
		
		try {
			out.print("<html><center><body><h1>Insert New Student</h1>");
			   out.print("<form action = 'InsertNewStudent' method = 'post'>");
			   out.print("<br>Name<input type = 'text' name = 'name' value = '' />");
			   out.print("<br>RegNo<input type = 'text' name = 'regno' value = '' />");
			   out.print("<br>Marks<input type = 'text' name = 'Marks' value = '' />");
			   
			   
			   out.print("<br><input type = 'submit' value = 'insert' />");
			   
			   out.print("</form></body></center></html>");
		}
		
		catch (Exception e)
		{ 
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		try {
			String n1 = request.getParameter("name");
			int n2 = Integer.parseInt(request.getParameter("regno"));
			int n3 = Integer.parseInt(request.getParameter("Marks"));
			
			Connection co = Dbconnect.getConnection();
			PreparedStatement ps = co.prepareStatement("insert into student (name,id,marks) values (?,?,?) ");
			ps.setString(1,n1);ps.setInt(2, n2);ps.setInt(3,n3);
			int rs = ps.executeUpdate();
			
			
			
		}
	catch(Exception e) {
		
		
	}

}
	
}


