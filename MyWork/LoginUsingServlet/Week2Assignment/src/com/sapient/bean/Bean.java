package com.sapient.bean;

public class Bean {

	private String id;
	private String name;
	private String location;
	private String availability;
	public Bean(String id,String name, String location, String availability) {
		super();
		this.id = id;
		this.name = name;
		this.location = location;
		this.availability = availability;
	}
	public Bean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Bean [name=" + name + ", location=" + location + ", availability=" + availability + "]";
	}
	
	
	
	
}
