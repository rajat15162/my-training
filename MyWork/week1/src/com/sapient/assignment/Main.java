package com.sapient.assignment;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		IntArray obj = new IntArray(4);
		obj.read();
		System.out.println(obj.avg());
		System.out.println(obj.search(5));
		obj.display();
		obj.sort();
		obj.display();
		
		IntArray obj1 = new IntArray(new int[] {4,6,7,8});
		obj1.display();
		
		IntArray obj2 = new IntArray(obj);
		obj2.display();
		
		

	}

}
