package com.sapient.assignment;

import java.util.Arrays;

import com.sapient.week1b.Read;

public class IntArray {
	
	private int [] arr;
	
	
     public IntArray() {
	
	arr = new int[10];
		
	}
     
     
	public IntArray(int size){
		
		arr = new int[size];
		
		
	}
	
	public IntArray(IntArray A) {
		
		this.arr = A.arr;
	}
	
public IntArray(int x[]) {
		
		arr = x;
	}
	
	public void display() {
		
		for(int i=0;i<arr.length;i++) {
			
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
	
public void display(int x) {
		
		for(int i=0;i<arr.length;i++) {
			
			System.out.println(arr[i]);
		}
		
	}
	
	
	
	public float avg() {
		
		float avg;
		float sum=0;
	     for(int i=0;i<arr.length;i++) {
	    	 
	    	 sum = sum+arr[i];
	     }
	     
	     avg = sum/arr.length;
	     
	     return avg;
	}
	
	public void sort() {
		
		
		Arrays.sort(arr);
	}
	
	public void read() {
		
		System.out.println("Enter " + arr.length + " elements." );
		for(int i=0;i<arr.length;i++) {
			
			arr[i] = Read.sc.nextInt();
		}
	}
	
	public boolean search(int x) {
		
		for(int i=0;i<arr.length;i++) {
			
			if(arr[i]==x)
				return true;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
