// demonstrate run time polymorphism

package com.sapient.week1b;

public class Demo2 {

	public static void main(String[] args) {
		
		System.out.println("Enter two numbers");
		Algebra ob = new Add();
		ob.setA(Read.sc.nextInt());
		ob.setB(Read.sc.nextInt());
		
		ob.calculate();
		ob.display();
		
		ob = new Subtract();
		ob.setA(Read.sc.nextInt());
		ob.setB(Read.sc.nextInt());
		
		ob.calculate();
		ob.display();
		
		
		
	}

}
