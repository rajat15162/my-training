// demonstrating compile time polymorphism

package com.sapient.week1b;

public class arithmetic {
	
	//signature
	// name of the method
	// number of arg
	// order of arg
	// type of arg
	// return type is not part of sign
	
	public int add(int a,int b) {
		return a+b;
	}
	public float add(float a, float b) {
		return a+b;
	}

}
