// demonstrates inheritance

package com.sapient.week1b;

public class Add extends Algebra{
	
	public void calculate() {
		
		setC(getA()+getB());
	}

}
