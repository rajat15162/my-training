package com.sapient.week1b;

public abstract class Algebra {

	private int a;
	private int b;
	private int c;
	
	abstract void calculate();
	
	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}
	public int getC() {
		return c;
	}
	public void setC(int c) {
		this.c = c;
	}
	
	void display() {
		
		System.out.println(c);
	}
	
}
