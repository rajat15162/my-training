package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service // automatically object is created at startup
public class StudentList {
	
	private List<StudentBean> list;

	public StudentList() {
		super();
		list = new ArrayList<>();
		list.add(new StudentBean("rajat",21,"delhii"));
		list.add(new StudentBean("mohit",22,"diu"));
		list.add(new StudentBean("satya",23,"lahore"));
		list.add(new StudentBean("puneet",24,"pune"));
	}

	public List<StudentBean> getList() {
		return list;
	}

	public void setList(List<StudentBean> list) {
		this.list = list;
	}
	
	
	

}
