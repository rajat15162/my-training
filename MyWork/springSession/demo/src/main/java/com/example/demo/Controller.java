package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
public class Controller {
	
	@Autowired//studentDAO  ka object inject ho gaya
	StudentDAO ob;
	
@RequestMapping("/hello")
public String display() {
	
	
	
	return "Welcome to Microservice!";
}



@RequestMapping("/hello1")
public StudentBean display1() {
	
	return new StudentBean("rajat",1,"delhi");
	
	
}	

@RequestMapping("/student")
public List<StudentBean> getStudents(){
	
	return ob.getStudents();
}

@RequestMapping("/student/{name}")
public List<StudentBean> getStudents2(@PathVariable String name){
	
	return ob.getDetails(name);
}

@RequestMapping(method=RequestMethod.POST,value = "/studentInsert")
public String insert(@RequestBody StudentBean bean){
	
	return ob.insert(bean);
}

@RequestMapping(method=RequestMethod.POST,value = "/studentDel/{name}")
public String delete(@PathVariable String name){
	
	return ob.delete(name);
}

@RequestMapping(method=RequestMethod.POST,value = "/studentDelBody")
public String delete1(@RequestBody String name){
	
	return ob.delete(name);
}


@RequestMapping(method=RequestMethod.PUT,value = "/studentPutBody")
public String update(@RequestBody StudentBean a){
	
	return ob.update1(a,a.getName());
}


}
