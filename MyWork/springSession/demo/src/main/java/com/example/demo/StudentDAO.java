package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service   // automatically object is created at startup
public class StudentDAO {
	
	@Autowired //student list ka object inject ho gaya
	StudentList l1;
	
	public List<StudentBean> getStudents(){
		
		
		return l1.getList();
	}
	
	
	public List<StudentBean> getDetails(String name) {
		
		
		return l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList());
	}
	
	
	public String insert(StudentBean ob) {
		
		l1.getList().add(ob);
		return "Added Successfully";
		
		
	}
	
public String delete(String name) {
		
		l1.getList().removeIf(e->e.getName().equals(name));
		return "Deleted Successfully";
		
		
	}

public String update1(StudentBean ob,String name) {
	
	l1.getList().removeIf(e->e.getName().equals(name));
	l1.getList().add(ob);
	
	return "updated Successfully";
	
	
}

}
