package com.example.demo;

import java.io.Serializable;

public class Add implements Serializable{

	
	transient int num1; // this will not be stored now
	int num2;
	int num3;
	
	public void setData(int num1,int num2) {
		
		this.num1 = num1;
		this.num2 = num2;
		
	}
	
	public void cal() {
		
		num3 = num1+num2;
		
	}
	
	public void display() {
		
		System.out.println("num1="+num1+"num2="+num2+ "num3= "+num3);
	}
}
