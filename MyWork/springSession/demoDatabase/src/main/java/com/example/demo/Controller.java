package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
public class Controller {

	@Autowired
	StudentDetailDAO sdetails;
	
	@Autowired
	StudentMarksDAO smarks;
	
	@PostMapping("/addS")
	public String storeStudentDetails(@RequestBody StudentDetails ob) {
		
		
		sdetails.save(ob);
		return "Saved";
		
		
	}
	
	
	@PostMapping("/addM")
	public String storeStudentMarks(@RequestBody StudentMarks ob) {
		
		
		smarks.save(ob);
		return "Saved Marks";
		
		
	}
	
	@GetMapping("/displayS")
	public List<StudentDetails> display(){
		
		return (List<StudentDetails>)sdetails.findAll();
	}
	
	@GetMapping("/displayM")
	public List<StudentMarks> displayM(){
		
		return (List<StudentMarks>)smarks.findAll();
	}
}
