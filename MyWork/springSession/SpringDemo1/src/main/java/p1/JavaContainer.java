package p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
@Configuration
public class JavaContainer {
	@Bean
	@Scope("singleton")
	public Hello get1() {
		
		return new Hello();
	}
	
	
	@Bean
	public Employee get2() {
		
		
		Employee ob;
		ob  = new Employee("Rajat",21);
		return ob;
		
	}
	
	@Bean
	public ListOfHolidays get3() {
		ListOfHolidays ob;
		ob = new ListOfHolidays();
		ob.getHolidays().add(new Holiday("New Year","1/1/19"));
		ob.getHolidays().add(new Holiday("Independence day","15/1/19"));
		
		return ob;
		
		
	}
	
	
	
	

}
