package p1;

import java.util.ArrayList;
import java.util.List;

public class ListOfHolidays {
	
	private List<Holiday> holidays = new ArrayList<Holiday>();

	public ListOfHolidays(List<Holiday> holidays) {
		super();
		this.holidays = holidays;
	}

	public List<Holiday> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<Holiday> holidays) {
		this.holidays = holidays;
	}

	@Override
	public String toString() {
		return "ListOfHolidays [holidays=" + holidays + "]";
	}

	public ListOfHolidays() {
		super();
	}
	

}
