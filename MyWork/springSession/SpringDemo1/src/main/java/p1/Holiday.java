package p1;

public class Holiday {

	private String holidayName;
	private String date;
	public String getHolidayName() {
		return holidayName;
	}
	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}
	public Holiday() {
		super();
	}
	public Holiday(String holidayName, String date) {
		super();
		this.holidayName = holidayName;
		this.date = date;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Holiday [holidayName=" + holidayName + ", date=" + date + "]";
	}
	
}
