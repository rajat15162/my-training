package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);
		Hello ob;
		ob = context.getBean(Hello.class);
		ob.display();
		
		Employee ob1;
		ob1  =context.getBean(Employee.class);
		System.out.println(ob1);
		
		ListOfHolidays ob2;
		ob2 = context.getBean(ListOfHolidays.class);
		System.out.println(ob2);
	}

}
