// what i want to check is my aspect

package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)   // weaving
public class ArithmeticAspect {
	
	
	@Before("execution(* *.*(double,double))")   // cross-cut or point-cut   and @before is advice
	public void check1(JoinPoint jpoint) {
		
		for(Object x : jpoint.getArgs()) {
			
			double v = (Double)x;
			if(v<0) {
				
				throw new IllegalArgumentException("No Negative no.");
			}
		}
	}

	
	
}
