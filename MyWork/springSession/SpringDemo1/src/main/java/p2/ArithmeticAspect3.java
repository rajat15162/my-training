package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;

@Aspect

public class ArithmeticAspect3 {
	
	
	@AfterReturning(pointcut="execution(* *.*(double,double))",returning = "result")
	public void check3(JoinPoint j, Object result) {
		
		if((Double)result > 50)
			throw new IllegalArgumentException("bhai 100 se bada kyu diya?");
		
		
		//System.out.println("Mai hu after mei" + result);
		
		
		
	}

}
