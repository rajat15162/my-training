package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(1)
public class ArithmeticAspect2 {

	
	@Before("execution(* *.*(double,double))")
	public void check2(JoinPoint jpoint) {
		
		for(Object x: jpoint.getArgs()) {
			
			double v = (Double)x;
			if(v>100) {
				
				throw new IllegalArgumentException("No. cannot be greater than 100");
			}
		}
		
		
	}
}
