package hibernateAspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class EmpAspect {

	
	@AfterReturning(value="execution(* *.sumsal())",returning="result")
	   public void check4(JoinPoint j , Object result) {
		
		System.out.println("aara h");
		if((Long)result>1000)
			throw new IllegalArgumentException("Bhai sal ka sum 1000 se bada ho gaya hai");
		
	}
}
