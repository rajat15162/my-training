package hibernateAspect;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import net.bytebuddy.asm.Advice.ArgumentHandler.Factory;
import p2.Arithmetic;

public class Demo {

	
	public static void main(String[] args) {
		
		
		try {
			
			EmpInterface ob;
			ApplicationContext context =new ClassPathXmlApplicationContext("bean.xml");
			ob = (EmpInterface)context.getBean("empp");
			System.out.println(ob.sumsal());
			
		}catch(Exception e) {
			
			System.out.println(e.getMessage());
		}
		
		
		/*EmpInterface em = new EmpImplementation();
		em.sumsal();
		*/
	
		
	}
}
