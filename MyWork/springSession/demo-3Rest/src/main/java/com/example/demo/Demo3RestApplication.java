package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo3RestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Demo3RestApplication.class, args);
	}

}
