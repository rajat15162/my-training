package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo3ResttApplication {

	public static void main(String[] args) {
		SpringApplication.run(Demo3ResttApplication.class, args);
	}

}
