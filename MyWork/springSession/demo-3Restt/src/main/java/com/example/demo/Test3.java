//insert

package com.example.demo;

import java.util.Scanner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
class Read{    
	public static Scanner sc = new Scanner(System.in);
	}
public class Test3 
{      
	static final String URL_CREATE_EMPLOYEE = "http://10.151.60.137:8099/students/studentInsert";        
	public static void main(String[] args) {               
		Studentbean newEmployee = new Studentbean();          
		System.out.println("enter name, age , city");               
		newEmployee.setName(Read.sc.next());     
		newEmployee.setAge(Read.sc.next());     
		newEmployee.setCity(Read.sc.next());               
		HttpHeaders headers = new HttpHeaders();          
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);         
		headers.setContentType(MediaType.APPLICATION_JSON);        
		RestTemplate restTemplate = new RestTemplate();       
		// Data attached to the request.      
		HttpEntity<Studentbean> requestBody = new HttpEntity<>(newEmployee, headers);     
		// Send request with POST method.     
		String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);               
		System.out.println("Employee created");
		
	}
	
}