// delete

package com.example.demo;



	
	import java.util.Scanner;
	import org.springframework.http.HttpEntity;
	import org.springframework.http.HttpHeaders;
	import org.springframework.http.MediaType;
	import org.springframework.web.client.RestTemplate;
	
	public class Test4
	{      
		static final String URL_CREATE_EMPLOYEE = "http://10.151.60.137:8099/students/studentDelBody";        
		public static void main(String[] args) {               
			String newEmployeeName;          
			System.out.println("enter name to be deleted");               
			newEmployeeName=Read.sc.next();                   
			HttpHeaders headers = new HttpHeaders();          
			headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);         
			headers.setContentType(MediaType.APPLICATION_JSON);        
			RestTemplate restTemplate = new RestTemplate();       
			// Data attached to the request.      
			HttpEntity<String> requestBody = new HttpEntity<>(newEmployeeName, headers);     
			// Send request with POST method.     
			String e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, String.class);               
			System.out.println("Employee deleted");
			
		}
		
	}

