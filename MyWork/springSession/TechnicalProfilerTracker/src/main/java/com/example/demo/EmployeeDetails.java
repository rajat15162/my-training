package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "employeeDetails")

public class EmployeeDetails {

	@Id
	private String eid;
	@Column
	private String name;
	public EmployeeDetails() {
		super();
	}
	public EmployeeDetails(String eid, String name, String dob) {
		super();
		this.eid = eid;
		this.name = name;
		this.dob = dob;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	@Column
	private String dob;
	
	
	@Transient
	Set<EmployeeDetails> sm = new HashSet<>();
	
	@OneToMany(mappedBy="eid")
	public Set<EmployeeDetails> getSm() {
		return sm;
	}
	
	public void setSm(Set<EmployeeDetails> sm) {
		this.sm = sm;
	}
	
	
	
}
