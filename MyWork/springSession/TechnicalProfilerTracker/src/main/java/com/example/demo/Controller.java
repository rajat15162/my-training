package com.example.demo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
@Transactional

public class Controller {

	@Autowired
	EmployeeDetailsDAO edetails;
	
	@Autowired
	ProjectDetailsDAO pdetails;
	
	@PostMapping("/addE")
	public String storeEmployeeDetails(@RequestBody EmployeeDetails ob) {
		
		
		edetails.save(ob);
		return " Employee Saved";
		
		
	}
	
	
	@PostMapping("/addP")
	public String storeProjectDetails(@RequestBody ProjectDetails ob) {
		
		
		pdetails.save(ob);
		return "Saved project";
		
		
	}
	
	@GetMapping("/displayE")
	public List<EmployeeDetails> displayEmployeeDetails(){
		
		return (List<EmployeeDetails>)edetails.findAll();
	}
	
	@GetMapping("/displayP")
	public List<ProjectDetails> displayProjectDetails(){
		
		return (List<ProjectDetails>)pdetails.findAll();
	}
	
	
	
	 @DeleteMapping("/deleteE")
	 public String deleteEmployee(String id) {
		 
		 edetails.deleteById(id);
		 return "Deleted Successfully";
	 }

	 @DeleteMapping("/deleteEByName")
	 public long deleteEmployeeByName(String name) {
		 
		 return edetails.deleteByName(name);
		 
	 }
	
}
