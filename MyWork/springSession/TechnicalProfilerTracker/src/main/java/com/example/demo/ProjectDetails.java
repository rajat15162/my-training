package com.example.demo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ProjectDetails")

public class ProjectDetails {

	@Id
	private String pid;
	@Column
	private String eid;
	@Column
	private String pname;
	
	
	
	@Transient
	@OneToMany()
	@JoinColumn(name="eid")
	List<EmployeeDetails> sref;
	
	
	public List<EmployeeDetails> getSref() {
		return sref;
	}
	public void setSref(List<EmployeeDetails> sref) {
		this.sref = sref;
		
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
}
