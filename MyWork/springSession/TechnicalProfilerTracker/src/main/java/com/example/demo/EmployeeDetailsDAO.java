package com.example.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;


@Service
public interface EmployeeDetailsDAO extends CrudRepository<EmployeeDetails, String> {
	
	public long deleteByName(String name);

}
