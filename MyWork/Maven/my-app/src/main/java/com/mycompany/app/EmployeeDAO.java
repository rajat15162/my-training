package com.mycompany.app;

import java.util.List;
import java.util.stream.Collectors;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class EmployeeDAO {

	
	public static List<EmployeeBean> readData(){
		
		List<EmployeeBean> lo = new ArrayList<EmployeeBean>();
		
		
		try {
			  
		    BufferedReader reader;
		    reader = new BufferedReader(new FileReader("C:\\Users\\rajaggar2\\Desktop\\rnew\\my-training\\MyWork\\Week2Day1\\src\\com\\sapient\\firstassessment\\test2.csv"));
		      
		    String line;
		    EmployeeBean employeeBean = null;
		     
		      while ((line = reader.readLine())!=null) {

		        String[] token = line.split(",");
		        
		    
		        	
		        	employeeBean = new EmployeeBean(Integer.parseInt(token[0]),token[1],Integer.parseInt(token[2]));
		        	lo.add(employeeBean);	     
		        
		      
		      
		      }     
		      reader.close();
		      
		} catch (IOException e1) {
		      e1.printStackTrace();
		    }
			  catch(Exception e2) {
				  
			  }
			  
			  catch(Throwable e3) {
				  
			  }
		  
		
		return lo;
	}
	
	
	public static int getTotsal(List<EmployeeBean> l) {
		
		
		int salary;
		
		salary = l.stream().mapToInt(x->x.getSalary()).sum();
		
		//System.out.println("tot sal"+salary);
		
		
		return salary;
		
		
		
	}
	
	
	
	public static int getCount (List<EmployeeBean> l , int salary) {
		
		long n;
		n= l.stream().filter(x->x.getSalary()==salary).count();
		return (int)n;
	}
	
	
	public static EmployeeBean getEmployee(List<EmployeeBean> l ,int id) {
		
		
		List<EmployeeBean> e;
		
		
		e = l.stream().filter((x)->(x.getId()==id)).collect(Collectors.toList());
	//	System.out.println(e.get(0));
		return e.get(0);
		
		
		
		
		
		
	}
	
	
	
	
	
	
}
