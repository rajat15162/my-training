package com.sapient.day5QuesJdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CricketDAO {
	
	public List<PlayerBean> getPlayers() throws Exception{
		Connection co = DbConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("select id,first_name,last_name,jersey_no from player");
		ResultSet rs = ps.executeQuery();
		//rs.next();
		//System.out.println(rs.getInt(1));
		List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public List<PlayerBean> getAnyPlayer(int id) throws Exception{
		Connection co = DbConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("select id,first_name,last_name,jersey_no from player where id =? ");
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public int insertPlayer(PlayerBean ob) throws Exception{
		Connection co = DbConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("insert into player (id,first_name,last_name,jersey_no) values (?,?,?,?) ");
		ps.setInt(1, ob.getId());ps.setString(2, ob.getFname());ps.setString(3, ob.getLname());ps.setInt(4, ob.getJerseyno());
		int rs = ps.executeUpdate();
		return rs;
	}
	
	
	public int deletePlayer(int id)throws Exception{
		
		Connection co = DbConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("delete from player where id = ?");
		ps.setInt(1, id);
		int rs = ps.executeUpdate();
		return rs;
		
	}
	
	public int updatePlayer(int id)throws Exception{
		
		Connection co = DbConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("update player set first_name ='Priya', last_name ='Mehra' where id=?");
		ps.setInt(1, id);
		int rs = ps.executeUpdate();
		return rs;
		
	}

}
