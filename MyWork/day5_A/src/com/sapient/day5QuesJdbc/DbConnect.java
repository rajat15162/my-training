package com.sapient.day5QuesJdbc;
import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnect {
	public static Connection co = null;
	public static Connection getConnection() throws Exception {
	if(co==null) {	
		Class.forName("oracle.jdbc.driver.OracleDriver");
		co = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","SYSTEM","system");
		System.out.println("Connection established");
	}
	
	return co;
	}
}
