package com.sapient;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;



public class CalTest {
	
	Calculator c = null;
	CalculatorService service = mock(CalculatorService.class);
	
	@Before
	public void setup() {
		
		c = new Calculator(service);
		
	}
	
	

	@Test
	public void test() {
		
		when(service.add(3, 6)).thenReturn(9);
		assertEquals(27, c.perform(3, 6));
		verify(service).add(3,6);
		
		
		
	}

}
