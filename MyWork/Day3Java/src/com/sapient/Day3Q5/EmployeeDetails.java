// employee dao i.e model

package com.sapient.Day3Q5;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmployeeDetails {
	
	Map<Integer , Employee> map = new HashMap<Integer,Employee>();
	
	public void insertData(Employee emp) {
		
		map.put(emp.getId(), emp);
		
	}
	
	public Employee getData(Integer key) {
		
		  return map.get(key);
	}
	
	
public List<Employee> getList(int i){
		
		List<Employee> L1 = map.values().stream().collect(Collectors.toList());
		
		if(i==0)
		Collections.sort(L1,(o,o1)->o.getName().compareTo(o1.getName()));
		
		else
			Collections.sort(L1,(o,o1)->o.getAge()-o1.getAge());
		
		
		return L1;
	}

}
