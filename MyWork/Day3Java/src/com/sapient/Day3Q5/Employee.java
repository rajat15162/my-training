// pojo class for data ie. employeeBean

package com.sapient.Day3Q5;

import java.util.Comparator;

public class Employee  {
	
	
	private int id;
	private String name;
	private int age;
	
	public Employee() {
		
		super();   // present inside default constructor
	}
	
	public Employee(int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	/*@Override
	public int compareTo(Employee o) {
		// TODO Auto-generated method stub
		return this.name.compareTo(o.name);
		//return this.age-o.age;
	}*/
	
	
	public String toString() {
		
		
		return id+" "+name+" "+ age;
	}

	
	
	
	
	
	

}
