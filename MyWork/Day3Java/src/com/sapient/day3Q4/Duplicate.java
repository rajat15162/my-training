package com.sapient.day3Q4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Duplicate {

	public static void main(String[] args) {
	
		List<Integer> list = new ArrayList<Integer>(Arrays.asList(1,1,3,4,5,6,7,7,7));
		
		System.out.println(list);
		
		List<Integer> l2 = list.stream().distinct().collect(Collectors.toList());
		
	    l2.forEach(p->System.out.println(p));
		
		

	}

}
