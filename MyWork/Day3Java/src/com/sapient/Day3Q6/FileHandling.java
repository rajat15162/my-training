package com.sapient.Day3Q6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class FileHandling {

  public static void main(String[] args) {
	  
	  try {
		  
    BufferedReader reader;

    
      reader = new BufferedReader(new FileReader(
          "C:\\Users\\rajaggar2\\Desktop\\rr\\my-training\\MyWork\\Day3Java\\src\\com\\sapient\\Day3Q6\\test"));
      BufferedWriter fw = new BufferedWriter(new FileWriter("C:\\Users\\rajaggar2\\Desktop\\rr\\my-training\\MyWork\\Day3Java\\src\\com\\sapient\\Day3Q6\\test2"));
      String line;
     
      while ((line = reader.readLine())!=null) {

        String[] token = line.split(",");
        int sum =0;
        sum = Arrays.stream(token).mapToInt(num -> Integer.parseInt(num.trim())).sum();
        line = line + " = " + sum;
        //System.out.println(sum);
        fw.write(line);
        fw.newLine();
      }
      reader.close();
      fw.close();
      System.out.println("File is processed");
    } catch (IOException e) {
      e.printStackTrace();
    }
	  catch(Exception e) {
		  
	  }
	  
	  catch(Throwable e) {
		  
	  }
  }
}