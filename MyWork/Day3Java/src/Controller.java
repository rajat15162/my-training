

// demo class is the controller
public class Controller {

	
	public static void main(String[] args) {
		
		AddBean addbean = new AddBean();
		
		View view  = new View();              // view->controller->model->controller->view
		AddDAO adddao = new AddDAO();
		view.readData(addbean);   // read
		adddao.compute(addbean);   // compute
		view.display(addbean);    // display
		
		addbean = null;
		view = null;
		adddao = null;
		
		System.gc();
		
		
		
		
	}
	
	 public void finalize() 
	    { 
	        System.out.println("finalize method overriden"); 
	    } 
	
	
	
}
