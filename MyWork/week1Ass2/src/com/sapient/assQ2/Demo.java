package com.sapient.assQ2;

public class Demo {

	public static void main(String[] args) {
	
		Mat o1 = new Mat();
		o1.read();
		Mat o2 = new Mat(new int[][] {{1,2,3},{4,5,6},{7,8,9}});
		Mat o3 = o1.add(o2);
		
		o1.display();
		o2.display();
		o3.display();
		
		
		}

	}


