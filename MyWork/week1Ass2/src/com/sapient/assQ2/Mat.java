package com.sapient.assQ2;


public class Mat {

	private int mat[][];
	public Mat() {
		// TODO Auto-generated constructor stub
		
		mat = new int[3][3];
	}
	
	public Mat(int m,int n) {
		
		mat = new int[m][n];
		
	}

	

	public Mat(int[][] m) {
		mat = m;
	}
	
	public Mat(Mat ob) {
		
		this.mat = ob.mat;
	}
	
	public void read() {
		
		
		System.out.println("Enter the matrix of size" + mat.length +" "+ mat[0].length);
		for(int i=0;i<mat.length;i++) {
			for(int j=0;j<mat[0].length;j++) {
				
				mat[i][j] = Read.sc.nextInt();
			}
		}
	}
	
	
public void display() {
		
		
		
		for(int i=0;i<mat.length;i++) {
			for(int j=0;j<mat[0].length;j++) {
				
				System.out.print(mat[i][j]);
			}
			System.out.println();
		}
		
		System.out.println();
	}
	

public Mat add(Mat m) {
	
	
	Mat temp = new Mat(m.mat.length,m.mat[0].length);
	for(int i=0;i<m.mat.length;i++) {
		for(int j=0;j<m.mat[0].length;j++) {
			
			temp.mat[i][j] = mat[i][j] + m.mat[i][j];
			
		}
	}
	
	return temp;
	

}
	
	
	
	
	
	

}
