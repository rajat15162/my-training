package com.sapient.AssQ4;

public class Prime {
	
	
	
	public static boolean check (int x)
	{
		if(x==1)
			return false;
		
		if(x==2)
		  return true;
		
		else {
			
			for(int i=2;i<=Math.sqrt(x);i++) {
				
				if(x%i==0)
					return false;
			}
		}
		
		
		return true;
	}

}
