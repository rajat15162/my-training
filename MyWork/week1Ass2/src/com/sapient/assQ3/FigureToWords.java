package com.sapient.assQ3;

public class FigureToWords {

	public FigureToWords() {
		
	}
		
		public static String getWords(long amt) {
			
			String word = "";
			String[] unit= {"","One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ", "nine ","ten ", "eleven ", "twelve ",  
                    "thirteen ", "fourteen ", 
                    "fifteen ", "sixteen ",  
                    "seventeen ", "eighteen ", "nineteen "};
			String[] tens = {"","Ten ","Twenty ","thirty ", "forty ", "fifty ", 
                    "sixty ", "seventy ", "eighty ", "ninety "};
			String[] vunit = {"crores ","lakhs ","thousands ","hundreds ","only "};
			
			long[] nunit = {10000000L,100000L,1000,100,1};
			
			for(int i=0;i<nunit.length;i++) {
				
				int n = (int)(amt/nunit[i]);
				amt = amt%nunit[i];
				if(n>0) {
					if(n>19) {
						word+=tens[n/10] + unit[n%10] + vunit[i];
					}
					else {
					    word+=unit[n]+vunit[i];	
					}
				}
			}
			
			return word;
		}
	}


