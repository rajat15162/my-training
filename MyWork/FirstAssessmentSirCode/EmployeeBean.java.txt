
public class EmployeeBean {
private String name;
private int ID;
private float salary;

public EmployeeBean(String name, int iD, float salary) {
	super();
	this.name = name;
	ID = iD;
	this.salary = salary;
}

@Override
public String toString() {
	return "EmployeeBean [name=" + name + ", ID=" + ID + ", salary=" + salary + "]";
}

public EmployeeBean() {
	super();
}

public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getID() {
	return ID;
}
public void setID(int iD) {
	ID = iD;
}
public float getSalary() {
	return salary;
}
public void setSalary(float salary) {
	this.salary = salary;
}
@Override
	public int hashCode() {
	int x = (ID*2)+name.hashCode()+(int)salary;
	//return this.toString().hashCode();
return x;
	}
public boolean equals(EmployeeBean emp) {
	if(this.hashCode() == emp.hashCode())
	return true;
	else 
		return false;
}
}
