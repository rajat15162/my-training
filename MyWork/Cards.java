package day2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

class Game{
 public int flag=0;
 public synchronized void g1(List<Integer> deck) {
  try {
   if(flag==0)
   {
    flag=1;
    
   Thread.sleep(500);
   System.out.println("P1-"+deck.get(0));
   deck.remove(0);
   notifyAll();
   
   
   wait();
   }
   else
   {
    wait();
   }
  } catch (InterruptedException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
 }
 
 public synchronized void g2(List<Integer> deck) {
  try {
   if(flag==1)
   {flag=2;
    
   Thread.sleep(500);
   System.out.println("P2-"+deck.get(0));
   deck.remove(0);
   
   notifyAll();
   wait();
   }
   else
   {
    wait();
   }
  } catch (InterruptedException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
  
 }
 public synchronized void g3(List<Integer> deck) {
  try {
   if(flag==2)
   {flag=0;
    
   Thread.sleep(500);
   System.out.println("P3-"+deck.get(0));
   deck.remove(0);
   
   notifyAll();
   wait();
   }
   else
   {
    wait();
   }
  } catch (InterruptedException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
 }
}
class Player1 extends Thread{
 List<Integer> deck = new ArrayList<>();
 Game game;
 public Player1(List<Integer> deck, Game game)
 {
  this.deck = deck;
  this.game = game;
 }
 public void run() {
  for(int i=0 ; i<17; i++)
  {
   game.g1(deck);
  }
 }
 
 
}
class Player2 extends Thread{
 List<Integer> deck = new ArrayList<>();
 Game game;
 public Player2(List<Integer> deck, Game game)
 {
  this.deck = deck;
  this.game = game;
 }
 public void run() {
  for(int i=0 ; i<17; i++)
  {
   game.g2(deck);
  }
 }
}
class Player3 extends Thread{
 List<Integer> deck = new ArrayList<>();
 Game game;
 public Player3(List<Integer> deck, Game game)
 {
  this.deck = deck;
  this.game = game;
 }
 public void run() {
  for(int i=0 ; i<17; i++)
  {
   game.g3(deck);
  }
 }
}
class Play{
 
}
public class Game1 {
 
 
 public static void shuffle(List<Integer> deck)
 {
  Collections.shuffle(deck);
  
 }
 public static void main(String[] args) {
  
  List<Integer> deck = new ArrayList<>();
  for(int i=0; i<=3; i++)
  {
  deck.add(1);deck.add(2);deck.add(3);deck.add(4);deck.add(5);deck.add(6);deck.add(7); deck.add(8);
  deck.add(9);deck.add(10);deck.add(11);deck.add(12);deck.add(13);
  }
  shuffle(deck);
  Game game = new Game();
  //for(int i=0; i<deck.size(); i++)
  // System.out.println(deck.get(i));
  List<Integer> deck1 = new ArrayList<>(deck.subList(0, 16));
  List<Integer> deck2 = new ArrayList<>(deck.subList(17, 33));
  List<Integer> deck3 = new ArrayList<>(deck.subList(34, 50));
  Player1 p1 = new Player1(deck1, game);
  Player2 p2 = new Player2(deck2, game);
  Player3 p3 = new Player3(deck3, game);
  p1.start();
  p2.start();
  p3.start();
  
  
  
 }
}